const path = require('path');
const { log } = require('console');
const express = require('express');
const router = express.Router();
const fs = require('fs');

const dirFile = path.join(__dirname);



/* GET home page. */
router.get('/', function(req, res) {
  const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
  res.render('index', { title: 'home-page', fullUrl});
  });

module.exports = router;
