const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    
    res.render('admin', { title: 'admin-page' })
})

router.post('/', (req, res) => {
    const { data } = req;
    res.send('admin', { title: 'admin-page'})
})


module.exports = router;